defmodule Bus do
  def number(stops) do
    calc(stops, 0)
  end

  def calc([{on, off} | tail], aggregate) do
    aggregate + calc(tail, on - off)
  end

  def calc([], aggregate) do
    aggregate
  end
end

IO.puts(Integer.to_string(Bus.number([{10,0},{3,5},{5,8}])))
IO.puts(Integer.to_string(Bus.number([{3,0}, {9,1}, {4,10}, {12,2}, {6,1}, {7,10}])))
IO.puts(Integer.to_string(Bus.number([{3,0}, {9,1}, {4,8}, {12,2}, {6,1}, {7,8}])))

